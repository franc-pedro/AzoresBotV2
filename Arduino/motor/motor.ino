#include "motor.h"

Motor motor(4,5,6,7,8,9);

void setup ()
{
  
  motor.setup ();

}

void loop ()
{
  motor.fwd (100, 100);
  delay (1000);
  motor.bwd (100, 100);
  delay (1000);  
}
