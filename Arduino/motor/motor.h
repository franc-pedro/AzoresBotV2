//Class for L298 Motor Driver
class Motor
{
private:
  int IN1, IN2, ENA;
  int IN3, IN4, ENB;

public:
    Motor (int in1, int in2, int in3, int in4, int in5, int in6)
  {
    IN1 = in1;
    IN2 = in2;
    ENA = in3;
    IN3 = in4;
    IN4 = in5;
    ENB = in6;
  }

  void setup ()
  {
    pinMode (IN1, OUTPUT);
    pinMode (IN2, OUTPUT);
    pinMode (ENA, OUTPUT);
    pinMode (IN3, OUTPUT);
    pinMode (IN4, OUTPUT);
    pinMode (ENB, OUTPUT);
  }

  void fwd (int speedL, int speedR)
  {
    digitalWrite (IN1, 1);
    digitalWrite (IN2, 0);
    digitalWrite (ENA, speedL);
    digitalWrite (IN3, 1);
    digitalWrite (IN4, 0);
    digitalWrite (ENB, speedL);
  }
  void bwd (int speedL, int speedR)
  {
    digitalWrite (IN1, 0);
    digitalWrite (IN2, 1);
    digitalWrite (ENA, speedL);
    digitalWrite (IN3, 0);
    digitalWrite (IN4, 1);
    digitalWrite (ENB, speedL);
  }

};
