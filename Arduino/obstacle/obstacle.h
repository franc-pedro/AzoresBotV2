class Obstacle
{
private:
  int TRIGGER;
  int ECHO;
  float duration, dist;

public:

    Obstacle (int echo, int trigger)
  {
    ECHO = echo;
    TRIGGER = trigger;
  }

  void setup ()
  {
    pinMode (TRIGGER, OUTPUT);
    pinMode (ECHO, INPUT);
  }

  float distance ()
  {
    digitalWrite (TRIGGER, LOW);
    delayMicroseconds (2);
    digitalWrite (TRIGGER, HIGH);
    delayMicroseconds (10);
    digitalWrite (TRIGGER, LOW);
    noInterrupts ();
    duration = pulseIn (ECHO, HIGH);
    interrupts ();
    dist = (duration * .0343) / 2;
    return dist;
  }

};
