#include "motorHR8833.h"

Motor motor(4,5,6,7);

void setup ()
{ 
  motor.setup ();
}

void loop ()
{
  motor.fwd (200, 200);
  delay (1000);
  motor.bwd (200, 200);
  delay (1000);
}
