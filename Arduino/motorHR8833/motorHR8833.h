//Class for HR8833 Motor Driver
class Motor
{
private:
  int MA1, MA2;
  int MB1, MB2;

public:

    Motor (int IN1, int IN2, int IN3, int IN4)
  {
    MA1 = IN1;
    MA2 = IN2;
    MB1 = IN3;
    MB2 = IN4;
  }

  void setup ()
  {
    pinMode (MA1, OUTPUT);
    pinMode (MA2, OUTPUT);
    pinMode (MB1, OUTPUT);
    pinMode (MB2, OUTPUT);
  }

  void fwd (int speedL, int speedR)
  {
    digitalWrite (MA1, speedL);
    digitalWrite (MA2, 0);
    digitalWrite (MB1, speedR);
    digitalWrite (MB2, 0);
  }
  void bwd (int speedL, int speedR)
  {
    int speed1 = 255 - speedL;
    digitalWrite (MA1, speed1);
    digitalWrite (MA2, 1);
    int speed2 = 255 - speedR;
    digitalWrite (MB1, speed2);
    digitalWrite (MB2, 1);
  }

};
